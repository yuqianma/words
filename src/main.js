// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-default/index.css'
import App from './App'
import VueResource from 'vue-resource'
import router from './router'

Vue.config.productionTip = false

const appId = 'UMg9O531pboi7hq0gJT4i1Yn-gzGzoHsz';
const appKey = 'WhPnUS6Xi6DUpY71lf2hDjeK';
AV.init({ appId, appKey });

Vue.use(VueResource)
Vue.use(ElementUI)

window._etys = {}

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
})
