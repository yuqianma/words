import Vue from 'vue'
import Router from 'vue-router'
import WordList from '@/components/WordList'
import Card from '@/components/Card'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'word-list',
      component: WordList
    },
    {
      path: '/card',
      name: 'card',
      component: Card
    }
  ]
})
